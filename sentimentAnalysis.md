# Sentiment analysis notebook
me  
`r format(Sys.Date(), "%a %d %B %Y")`  

(http://stackoverflow.com/questions/30957284/gitlab-rendering-rmd-instead-of-md-in-wiki#30967314)


[![Rdoc](http://www.rdocumentation.org/badges/version/tidytext)](http://www.rdocumentation.org/packages/tidytext)


```r
library(janeaustenr)
library(dplyr)
```

```
## 
## Attaching package: 'dplyr'
```

```
## The following objects are masked from 'package:stats':
## 
##     filter, lag
```

```
## The following objects are masked from 'package:base':
## 
##     intersect, setdiff, setequal, union
```

```r
original_books <- austen_books() %>%
  group_by(book) %>%
  mutate(linenumber = row_number()) %>%
  ungroup()

original_books
```

```
## # A tibble: 73,422 × 3
##                     text                book linenumber
##                    <chr>              <fctr>      <int>
## 1  SENSE AND SENSIBILITY Sense & Sensibility          1
## 2                        Sense & Sensibility          2
## 3         by Jane Austen Sense & Sensibility          3
## 4                        Sense & Sensibility          4
## 5                 (1811) Sense & Sensibility          5
## 6                        Sense & Sensibility          6
## 7                        Sense & Sensibility          7
## 8                        Sense & Sensibility          8
## 9                        Sense & Sensibility          9
## 10             CHAPTER 1 Sense & Sensibility         10
## # ... with 73,412 more rows
```

```r
library(tidytext)
tidy_books <- original_books %>%
  unnest_tokens(word, text)

tidy_books
```

```
## # A tibble: 725,054 × 3
##                   book linenumber        word
##                 <fctr>      <int>       <chr>
## 1  Sense & Sensibility          1       sense
## 2  Sense & Sensibility          1         and
## 3  Sense & Sensibility          1 sensibility
## 4  Sense & Sensibility          3          by
## 5  Sense & Sensibility          3        jane
## 6  Sense & Sensibility          3      austen
## 7  Sense & Sensibility          5        1811
## 8  Sense & Sensibility         10     chapter
## 9  Sense & Sensibility         10           1
## 10 Sense & Sensibility         13         the
## # ... with 725,044 more rows
```

```r
data("stop_words")
tidy_books <- tidy_books %>%
  anti_join(stop_words)
```

```
## Joining, by = "word"
```

```r
tidy_books %>%
  count(word, sort = TRUE) 
```

```
## # A tibble: 13,914 × 2
##      word     n
##     <chr> <int>
## 1    miss  1855
## 2    time  1337
## 3   fanny   862
## 4    dear   822
## 5    lady   817
## 6     sir   806
## 7     day   797
## 8    emma   787
## 9  sister   727
## 10  house   699
## # ... with 13,904 more rows
```

```r
library(tidyr)
get_sentiments("bing")
```

```
## # A tibble: 6,788 × 2
##           word sentiment
##          <chr>     <chr>
## 1      2-faced  negative
## 2      2-faces  negative
## 3           a+  positive
## 4     abnormal  negative
## 5      abolish  negative
## 6   abominable  negative
## 7   abominably  negative
## 8    abominate  negative
## 9  abomination  negative
## 10       abort  negative
## # ... with 6,778 more rows
```

```r
janeaustensentiment <- tidy_books %>%
  inner_join(get_sentiments("bing"), by = "word") %>% 
  count(book, index = linenumber %/% 80, sentiment) %>% 
  spread(sentiment, n, fill = 0) %>% 
  mutate(sentiment = positive - negative)
janeaustensentiment
```

```
## # A tibble: 920 × 5
##                   book index negative positive sentiment
##                 <fctr> <dbl>    <dbl>    <dbl>     <dbl>
## 1  Sense & Sensibility     0       16       26        10
## 2  Sense & Sensibility     1       19       44        25
## 3  Sense & Sensibility     2       12       23        11
## 4  Sense & Sensibility     3       15       22         7
## 5  Sense & Sensibility     4       16       29        13
## 6  Sense & Sensibility     5       16       39        23
## 7  Sense & Sensibility     6       24       37        13
## 8  Sense & Sensibility     7       22       39        17
## 9  Sense & Sensibility     8       30       35         5
## 10 Sense & Sensibility     9       14       18         4
## # ... with 910 more rows
```


```r
library(ggplot2)

ggplot(janeaustensentiment, aes(index, sentiment, fill = book)) +
  geom_bar(stat = "identity", show.legend = FALSE) +
  facet_wrap(~book, ncol = 2, scales = "free_x")
```

![](sentimentAnalysis_files/figure-html/unnamed-chunk-2-1.png)<!-- -->

If we want to analyze this with tidy tools, we need to transform it into a one-row-per-term data frame first with a tidy function. (For more on the tidy verb, see the broom package).


```r
library(tm)
```

```
## Loading required package: NLP
```

```
## 
## Attaching package: 'NLP'
```

```
## The following object is masked from 'package:ggplot2':
## 
##     annotate
```

```r
data("AssociatedPress", package = "topicmodels")
AssociatedPress
```

```
## <<DocumentTermMatrix (documents: 2246, terms: 10473)>>
## Non-/sparse entries: 302031/23220327
## Sparsity           : 99%
## Maximal term length: 18
## Weighting          : term frequency (tf)
```

```r
tidy(AssociatedPress)
```

```
## # A tibble: 302,031 × 3
##    document       term count
##       <int>      <chr> <dbl>
## 1         1     adding     1
## 2         1      adult     2
## 3         1        ago     1
## 4         1    alcohol     1
## 5         1  allegedly     1
## 6         1      allen     1
## 7         1 apparently     2
## 8         1   appeared     1
## 9         1   arrested     1
## 10        1    assault     1
## # ... with 302,021 more rows
```
We could find the most negative documents:

```r
ap_sentiments <- tidy(AssociatedPress) %>%
  inner_join(get_sentiments("bing"), by = c(term = "word")) %>%
  count(document, sentiment, wt = count) %>%
  ungroup() %>%
  spread(sentiment, n, fill = 0) %>%
  mutate(sentiment = positive - negative) %>%
  arrange(sentiment)
```
Or we can join the Austen and AP datasets and compare the frequencies of each word:

```r
comparison <- tidy(AssociatedPress) %>%
  count(word = term) %>%
  rename(AP = n) %>%
  inner_join(count(tidy_books, word)) %>%
  rename(Austen = n) %>%
  mutate(AP = AP / sum(AP),
         Austen = Austen / sum(Austen))
```

```
## Joining, by = "word"
```

```r
comparison
```

```
## # A tibble: 4,437 × 3
##          word           AP       Austen
##         <chr>        <dbl>        <dbl>
## 1   abandoned 2.097944e-04 7.093959e-06
## 2       abide 3.596475e-05 2.837584e-05
## 3   abilities 3.596475e-05 2.057248e-04
## 4     ability 2.937122e-04 2.128188e-05
## 5      abroad 2.397650e-04 2.553825e-04
## 6      abrupt 3.596475e-05 3.546980e-05
## 7     absence 9.590601e-05 7.874295e-04
## 8      absent 5.394713e-05 3.546980e-04
## 9    absolute 6.593538e-05 1.844429e-04
## 10 absolutely 2.097944e-04 6.739262e-04
## # ... with 4,427 more rows
```

```r
library(scales)
ggplot(comparison, aes(AP, Austen)) +
  geom_point() +
  geom_text(aes(label = word), check_overlap = TRUE,
            vjust = 1, hjust = 1) +
  scale_x_log10(labels = percent_format()) +
  scale_y_log10(labels = percent_format()) +
  geom_abline(color = "red")
```

![](sentimentAnalysis_files/figure-html/unnamed-chunk-6-1.png)<!-- -->




```r
janeaustensentiment <- 
  tidy_books %>%
  inner_join(get_sentiments("bing"), by = "word") %>% 
  count(book, index = linenumber %/% 80, sentiment) %>% 
  spread(sentiment, n, fill = 0) %>% 
  mutate(sentiment = positive - negative)
```



# Sentiment Analysis

+ http://michaeltoth.me/sentiment-analysis-of-warren-buffetts-letters-to-shareholders.html
    - In this post I'm going to be performing a sentiment analysis of the text of Warren Buffett's letters to shareholders from 1977 - 2016. A sentiment analysis is a method of identifying and quantifying the overall sentiment of a particular set of text. Sentiment analysis has many use cases, bu a common one is to determine how positive or negative a particular text document is, which is what I'll be doing here. 
    - For this, I'll be using bing sentiment analysis, developed by Bing Liu of the University of Illinois at Chicago. For this type of sentiment analysis, you first split a text document into a set of distinct words, and then for each word determining whether it is positive, negative, or neutral.



