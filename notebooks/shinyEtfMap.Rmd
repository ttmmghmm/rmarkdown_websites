---
title: "pulling-and-displaying-etf-data with shiny"
output: html_notebook
---

+ [pulling-and-displaying-etf-data](https://www.rstudio.com/rviews/2016/12/14/reproducible-finance-with-r-pulling-and-displaying-etf-data/)

```{r path back to the clean data}
# Let's build a dataframe to store these ticker symbols, country names and YTD numbers.
library(dplyr)
ticker <-  c("EWJ",  "EWZ",  "INDA", "FXI",  "EWG",  "EWC",  "EWY",  "EWT",  "EWU",  "EWH",  "EWA",
             "EWW",  "EWL",  "EWP", "EWS",  "EWI",  "EIDO", "ERUS", "ECH",  "EZA",  "THD",  "TUR",
             "EWD",  "EWQ",  "EWM",  "EPU",  "EWN",  "EPOL", "EPHE", "ENZL", "EIRL", "EWK",  "EIS",
             "EWO", "EDEN", "QAT", "UAE", "EFNL", "ENOR", "ICOL", "HEWY", "KSA")

name <-   c("Japan", "Brazil" ,"India", "China", "Germany" , "Canada", "Korea", "Taiwan", 
              "United Kingdom", "Hong Kong", "Australia", "Mexico", "Switzerland", "Spain", 
              "Singapore", "Italy", "Indonesia", "Russia", "Chile", "South Africa", "Thailand",  
              "Turkey", "Sweden", "France", "Malaysia", "Peru", "Netherlands", "Poland",
              "Philippines", "New Zealand", "Ireland", "Belgium", "Israel", "Austria","Denmark",
              "Qatar", "United Arab Emirates", "Finland", "Norway", "Colombia", "South Korea", 
              "Saudi Arabia")

ytd <- c(0.0358, 0.6314, -0.0140,  0.0721, -0.0289, 0.2198,  0.0729,  0.2029, -0.0467,  0.0897,
         0.0944, -0.1045, -0.0623, -0.0916,  0.0305, -0.1857,  0.1309,  0.3828,  0.1987,  0.1219,
         0.2458, -0.1053, -0.0052, -0.0199, -0.0410,  0.6015,  0.0017, -0.0481, -0.0408,  0.1394,
         -0.1183, -0.0428, -0.0432,  0.0462, -0.1078, -0.0244, 0.0570,  0.6397, -0.0146,  0.1424,
         0.1313,  0.0751) * 100

etf_ticker_country <- data_frame(ticker, name, ytd)

etf_ticker_country
```

## etf_ticker_country

We aren’t going to use the results of this import in the app. Rather, we are going to perform this import to test that we have the correct symbols, and that they play nicely with getSymbols(), because that is the function we will use in our Shiny app

```{r etf_ticker_country, eval = FALSE}
# getSymbols is part of the 'quantmod' package.

library(quantmod)

# Using getSymbols to import the ETF price histories will take a minute or two or 
# five - 42 time series is a lot of data. 

invisible(getSymbols(etf_ticker_country$ticker, auto.assign = TRUE, warnings = FALSE))

# Let select just the closing prices of the ETFs and merge them into a list.
# We'll use lapply for that purpose. Again, this is for testing purposes. It's not 
# going into production in our app.
etf_prices <- do.call(merge, lapply(etf_ticker_country$ticker, function(x) Cl(get(x))))

#Change the column names to the country names from our dataframe above.

colnames(etf_prices) <- etf_ticker_country$name

# Take a peek at the last 5 rows of each of the time series, 
# just to make sure it looks complete.

tail(etf_prices, n = 5)
```



need a shapefile that contains the spatial polygons for the countries of the world. The next code chunk will grab a shapefile from naturalearthdata.com. 
That shapefile has the longitude and latitude coordinates for the world’s countries and some data about them. 
We’ll then use the readOGR() function from the rgdal package to load the shapefile into our global environment.

```{r readOGR}
library(rgdal)
library(httr)

tmp <- tempfile()

invisible(GET("http://www.naturalearthdata.com/http//www.naturalearthdata.com/download/50m/cultural/ne_50m_admin_0_countries.zip", write_disk(tmp), overwrite =))
unzip(tmp, exdir = 'ne_50m_admin_0_countries')

world <-readOGR("./ne_50m_admin_0_countries", 'ne_50m_admin_0_countries', verbose = FALSE)
```
```{r}
head(world@data)
```

# leaflet

he leaflet package makes building a nice interactive map with these shapefiles relatively painless.

Before building a map, let’s make use of the data that was included in our data frame. The ‘gpd_md_est’ column (which you can see in the data frame above) contains GDP estimates for each country. We’ll add some color to our map with shades of blue that are darker for higher GDPs and lighter for lower GDPs.

```{r leaflet}

library(leaflet)

# Create a palette with different shades of blue for different
# GDP estimates.

gdpPal <- colorQuantile("Blues", world$gdp_md_est, n = 20)
```


```{r}
# Make a popup object.
# Notice we're referencing the column names with the '$', same as we would with a non-spatial dataframe.
economyPopup <- paste0("<strong>Country: </strong>", 
                world$name, 
                "<br><strong>Market Stage: </strong>", 
                 world$economy)
```

```{r}
# Build our leaflet map object.

leaf_world_economy <- leaflet(world) %>%
  addProviderTiles("CartoDB.Positron") %>% 
  setView(lng =  20, lat =  15, zoom = 2) %>%
      addPolygons(stroke = FALSE, smoothFactor = 0.2, fillOpacity = .7, color =
      ~gdpPal(gdp_md_est), layerId = ~name, popup = economyPopup)

# Display that object below.

leaf_world_economy

```


```{r}
library(sp)
# Once we run this line of code, our ticker symbols and ytd numbers will be added
# to the spatial dataframe.

world_etf <- merge(world, etf_ticker_country, by = "name")
```


```{r}
# Create a palette with different shades of blue for different
# year-to-date performances. Previously, we shaded by 'world$gdp_md_est', now
# we'll shade by 'world_etf$ytd'.

ytdPal <- colorQuantile("Blues", world_etf$ytd, n = 20)
```

```{r}
# Create a popup that displays the year-to-date performance.

ytdPopup <- paste0("<strong>Country: </strong>", 
                world_etf$name,
                "<br><strong> Year-to-date: </strong>", 
                world_etf$ytd, "%")
```

Now we’ll build another map that uses the year-to-date color scheme and popup, but we will make one more massively important change: we will change layerId = ~name to layerId = ~ticker to create a map layer of tickers.

Why is this massively important? When we eventually create a Shiny app, we want to pass ticker symbols to getSymbols() based on a user click. The ‘layerId’ is how we’ll do that: when a user clicks on a country, we capture the ‘layerId’, which is a ticker name that we can pass to getSymbols(). But that is getting ahead of ourselves. For now, here is the new map

```{r}
leaf_world_etf <- leaflet(world_etf) %>%
  addProviderTiles("CartoDB.Positron") %>% 
  setView(lng =  20, lat =  15, zoom = 2) %>%
      addPolygons(stroke = FALSE, smoothFactor = 0.2, fillOpacity = .7,
                  
      # The next line of code is really important for creating the map we want to use later.       
      
      color =~ytdPal(ytd), layerId = ~ticker, popup = ytdPopup)

leaf_world_etf

```

