# XGBoost Notebook
me  
`r format(Sys.Date(), "%a %d %B %Y")`  

(http://stackoverflow.com/questions/30957284/gitlab-rendering-rmd-instead-of-md-in-wiki#30967314)


# eXtreme Gradient Boosting is  

+  also called **XGBoost**
+  a __machine learning__ model 
+  an implementation of the gradient boosting framework. 
+ builds _predictive tree_-based models
+ **Boosting** 

    - an ensemble technique 
    - new models are added to correct the errors made by existing models. 
    -  Models are added sequentially until no further improvements can be made.
+ ensemble technique uses the tree ensemble model which is a set of classification and regression trees (CART). 
  - a single CART does not have a strong predictive power. 
  - a set of CART (i.e. a tree ensemble model) a sum of the predictions of multiple trees is considered.
+ Gradient boosting - new models predict the residuals or errors of prior models and then added together to make the final prediction

https://www.quantinsti.com/blog/forecasting-markets-using-extreme-gradient-boosting-xgboost/



```r
packgs <- c(
  "tidyquant", # "tidyverse" included
  "PerformanceAnalytics",
  "PortfolioAnalytics", 
  "xgboost"
)
library(dplyr)
```

```
## 
## Attaching package: 'dplyr'
```

```
## The following objects are masked from 'package:stats':
## 
##     filter, lag
```

```
## The following objects are masked from 'package:base':
## 
##     intersect, setdiff, setequal, union
```

```r
sapply(packgs, function(ii) 
  suppressPackageStartupMessages(
    library(ii, character.only = TRUE))) %>% 
  str
```

```
## List of 4
##  $ tidyquant           : chr [1:22] "tidyquant" "purrr" "readr" "tidyr" ...
##  $ PerformanceAnalytics: chr [1:22] "tidyquant" "purrr" "readr" "tidyr" ...
##  $ PortfolioAnalytics  : chr [1:24] "PortfolioAnalytics" "foreach" "tidyquant" "purrr" ...
##  $ xgboost             : chr [1:25] "xgboost" "PortfolioAnalytics" "foreach" "tidyquant" ...
```

# Get financial OHLC timeseries
## Map a character vector with multiple stock symbols

```r
c("AAPL", "GOOG", "FB") %>% 
  tq_get(
    get = "stock.prices", 
    to = today(),
    from = today() - months(5)
  ) -> 
  df
#```
#```{r}
# model's technical indicators
df %>% 
  mutate(
    rsi = RSI(close, n=14, maType="WMA"),
  ) -> df
df <- bind_cols(df, 
      as.data.frame(
        ADX(df[c("high", "low", "close")])) )
df <- bind_cols(df, 
      data.frame(
        sar = SAR(
          df[,c("high", "low")], 
          accel = c(0.02, 0.2))
      ) )
df %>% 
  mutate(trend = close - sar) ->
  df
```

```r
# create a lag in the technical indicators to avoid look-ahead bias 
rsi = c(NA,head(rsi,-1)) 
adx$ADX = c(NA,head(adx$ADX,-1)) 
trend = c(NA,head(trend,-1))
```

Working in returns and weights is different than working in trades and cash.

```r
# returns and weights v  and cash
# Example code
# install.packages("quantstrat",repos="http://R-Forge.R-project.org")
library(quantstrat)
startDate <- '2010-01-01'   # start of data
endDate <- '2010-07-31'     # end of data
symbols = c("XLF", "XLP", "XLE", "XLY", "XLV", "XLI", "XLB", "XLK", "XLU")
Sys.setenv(TZ = "UTC")      # set time zone
getSymbols(symbols, src = 'yahoo', index.class = c("POSIXt", "POSIXct"), from = startDate, to = endDate, adjust = TRUE)

#Initialise currency and instruments
initDate <- '2009-12-31'
initEq <- 1e6
currency("USD")
stock(symbols, currency = "USD", multiplier = 1)

#Create our Markowitz weights
m_wts <- data.frame('Date' = seq(as.Date("2010-01-31"), by="month", length.out=7))
invisible(lapply(symbols, function(x) m_wts[[x]] <<- as.numeric(NaN)))
invisible(lapply(1:nrow(m_wts), function(i) m_wts[i, 2:9] <<- round(runif(8)/8,2)))
invisible(lapply(1:nrow(m_wts), function(i) m_wts[i, 10] <<- 1 - sum(m_wts[i, 2:9])))
unlist(lapply(1:nrow(m_wts), function(i) sum(m_wts[i,2:10 ])))

initPortf(name = "markowitz", symbols, initDate = initDate)
initAcct(name="markowitz", portfolios="markowitz", initDate = initDate,

initEq = initEq)
initOrders(portfolio = "markowitz", initDate = initDate)
strategy('markowitz', store = T)
```


When you save the notebook, an HTML file containing the code and output will be saved alongside it (click the *Preview* button or press *Cmd+Shift+K* to preview the HTML file).
